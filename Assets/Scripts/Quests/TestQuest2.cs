﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestQuest2 : Quest
{
    public override bool CheckingPlacementCondition()
    {
        if (planet.questIsHere || completed == true || active == true || placed == true)
        {
            return false;
        }

        if (planet.type == Planet.planetTypes.Twin_Earth)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
