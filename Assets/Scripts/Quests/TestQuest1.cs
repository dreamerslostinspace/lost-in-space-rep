﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestQuest1 : Quest
{
    public override bool CheckingPlacementCondition()
    {
        if (planet.questIsHere || completed == true || active == true || placed == true)
        {
            return false;
        }

        if (planet.type == Planet.planetTypes.Molten)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool CheckingStartCondition()
    {
        if (completed == true || active == true)
        {
            return false;
        }

        if (playerContact)
        {
            Activation();
            return true;
        }

        return false;
    }
}
