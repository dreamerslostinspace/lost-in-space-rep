﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestsManager : MonoBehaviour
{
    [SerializeField]
    Quest[] quests;

    public bool CheckPlasementCondition(int id)
    {
        bool check = quests[id].CheckingPlacementCondition();

        if (check)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckStartCondition(int id)
    {
        bool check = quests[id].CheckingStartCondition();

        if (check)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckEndCondition(int id)
    {
        bool check = quests[id].CheckingEndCondition();

        return false;
    }

    public void QuestPlacement(int id)
    {
        quests[id].Placement();
    }

    public void QuestActivation(int id)
    {
        quests[id].Activation();
    }

    public void QuestCompletion(int id)
    {
        quests[id].Completion();
    }

    public void AddToQuestLog(int id)
    {
        quests[id].AddToQuestLog();
    }

    public void AddJournalEntry(int id)
    {
        quests[id].AddJournalEntry();
    }

    public void AwardReward(int id)
    {
        quests[id].AwardReward();
    }

    public int GetTotalNumberOfQuests()
    {
        return quests.Length;
    }

    public Quest GetQuest(int id)
    {
        return quests[id];
    }
}
