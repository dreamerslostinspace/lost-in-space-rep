﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest : MonoBehaviour
{
    public int iD;
    public string title;
    public string type;
    public int stage;
    public bool placed = false;
    public bool active = false;
    public bool completed = false;
    public QuestsManager questsManager;
    public GameObject place;

    public Planet planet = null;
    public bool playerContact;

    public virtual void Update()
    {
        CheckingStartCondition();
    }

    public virtual bool CheckingPlacementCondition()
    {
        if (planet.questIsHere || completed == true || active == true || placed == true)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public virtual bool CheckingStartCondition()
    {
        if (completed == true || active == true)
        {
            return false;
        }

        return false;
    }

    public virtual bool CheckingEndCondition() { return false; }

    public virtual void Placement()
    {
        this.placed = true;
    }

    public virtual void Activation()
    {
        this.active = true;
    }

    public virtual void Completion()
    {
        this.active = false;
        this.completed = true;
    }

    public virtual void AddToQuestLog() { }
    public virtual void AddJournalEntry() { }
    public virtual void AwardReward() { }
}
