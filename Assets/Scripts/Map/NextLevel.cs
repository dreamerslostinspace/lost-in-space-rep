﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public StarSystemGenerator ssg;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextLevelBut()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        Debug.Log("Next Scene");
        ssg.systemIsCreated = false;
        ssg.systemID = 100;
    }
}
