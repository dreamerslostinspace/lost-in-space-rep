﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedGenerator : MonoBehaviour
{
    public string seed;

    private string sunSettings;
    private int sunsCount;
    private int minNumberOfPlanets;
    private int maxNumberOfPlanets;
    private int asteroidFieldsCount;
    private int minNumberOfPOI;
    private int maxNumberOfPOI;
    private int minNumberOfEnemySpawners;
    private int maxNumberOfEnemySpawners;

    private int random;


    // Start is called before the first frame update
    void Start()
    {
        Generation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Generation()
    {
        random = Random.Range(1, 4);

        switch (random)
        {
            case 1:
                sunSettings = "red";
                break;
            case 2:
                sunSettings = "blue";
                break;
            case 3:
                sunSettings = "yellow";
                break;
        }

        seed += sunSettings;
        seed += "/";

        random = Random.Range(1, 4);

        switch (random)
        {
            case 1:
                sunsCount = 1;
                break;
            case 2:
                sunsCount = 2;
                break;
            case 3:
                sunsCount = 3;
                break;
        }

        seed += sunsCount.ToString();
        seed += "/";

        minNumberOfPlanets = Random.Range(1, 11);
        maxNumberOfPlanets = Random.Range(10, 16);

        seed += minNumberOfPlanets.ToString();
        seed += "/";
        seed += maxNumberOfPlanets.ToString();
        seed += "/";

        asteroidFieldsCount = Random.Range(0, 4);

        seed += asteroidFieldsCount.ToString();
        seed += "/";

        minNumberOfPOI = Random.Range(2, 4);
        maxNumberOfPOI = Random.Range(4, 6);

        seed += minNumberOfPOI.ToString();
        seed += "/";
        seed += maxNumberOfPOI.ToString();
        seed += "/";

        minNumberOfEnemySpawners = Random.Range(1, 2);
        maxNumberOfEnemySpawners = Random.Range(2, 4);

        seed += minNumberOfEnemySpawners.ToString();
        seed += "/";
        seed += maxNumberOfEnemySpawners;
        seed += "#";
    }

    public string GetSeed()
    {
        return seed;
    }
}
