﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Logic: 1) We determine how many planets we need and what type, 
//        2) Use a common planet pattern, generate a random place of the planet appearance (or use a fixed one when a certain type is required), 
//        3) Assign a planet type to the planet based on the distance from the sun, 
//        4) Choose the desired planet pattern 
//        5) Based on the type obtained we generate resources on it, 
//        6) We inform it of the location 
//        7) And place it in space.

public class StarSystemGenerator : MonoBehaviour
{
    public bool systemIsCreated;

    [Header("System Info")]
    public string systemName;
    public int systemID;
    public int numberOfSuns;
    public int numberOfPlanets;
    public int numberOfQuests;
    public int numberOfEvents;
    public int numberOfPOI;
    public int numberOfEnemySpawners;

    [Header("Main Generation Settings")]
    public bool usingInspector;
    public bool usingSeed;

    [Header("Seed")]
    public string seed;
    public GameObject seedGenerator;

    [Header("Managers")]
    public GameObject questsManager;
    public GameObject eventsManager;

    [Header("Sun generation settings - type")]
    public bool randomSun;
    public bool red;
    public bool blue;
    public bool yellow;

    [Header("Sun generation settings - count")]
    public bool randomSunCount;
    public bool one;
    public bool two;
    public bool three;

    [Header("Planet generation settings")]
    public int minNumberOfPlanets;
    public int maxNumberOfPlanets;
    [Range(0.0f, 100.0f)]
    public int addMoltenPlanets;
    [Range(0.0f, 100.0f)]
    public int addEarthLikePlanets;
    [Range(0.0f, 100.0f)]
    public int addTombPlanets;
    [Range(0.0f, 100.0f)]
    public int addGasGiantPlanets;
    [Range(0.0f, 100.0f)]
    public int addFrozenPlanets;

    [Header("Asteroid generation settings")]
    public bool randomFields;
    public bool internalField;
    public bool middleField;
    public bool externalField;

    [Header("POI generation settings")]
    public int minNumberOfPOI;
    public int maxNumberOfPOI;

    [Header("Enemy spawners generation settings")]
    public int minNumberOfEnemySpawners;
    public int maxNumberOfEnemySpawners;

    [Header("Resource generation settings")]
    public bool addHelium;
    public bool addIron;
    public bool addSilicium;
    public bool addGold;
    public bool addUranium;
    public bool addNuclearuel;
    public bool addWater;

    [Header("Prefabs of the suns")]
    public GameObject normalSun;

    [Header("Prefabs of the planets")]
    public Planet defaultPlanet;
    public Planet moltenPlanet;
    public Planet twinEarthPlanet;
    public Planet gasGiantPlanet;
    public Planet frozenPlanet;
    public Planet tombPlanet;

    [Header("Prefab of the asteroid")]
    public GameObject asteroid;

    [Header("Prefab of the POI")]
    public POI pOIPrefab;

    [Header("Prefab of the enemy spawner")]
    public EnemySpawner enemySpawnerPrefab;

    // PRIVATE
    private int requestedPlanets;
    private int randomNumberOfPlanets;

    private int requestedSuns;

    private int requestedPOI;
    private int randomNumberOfPOI;
    private int pOINearPlanet;
    private int pOIInSpace;
    private int pOIIdCounter;

    private int requestedEnemySpawners;
    private int randomNumberOfEnemySpawners;

    private QuestsManager questsManagerScript;
    private int questsTotalNumber;

    private EventsManager eventsManagerScript;
    private int eventsTotalNumber;

    private SeedGenerator seedGeneratorScript;

    private GameObject systemCenter;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this);

        requestedPlanets = 0;
        requestedSuns = 0;
        requestedPOI = 0;
        pOIIdCounter = 0;

        if (!usingSeed)
        {
            usingInspector = true;
        }

        // Seed Generator
        seedGeneratorScript = seedGenerator.GetComponent<SeedGenerator>();

        // Quest Manager
        questsManagerScript = questsManager.GetComponent<QuestsManager>();
        questsTotalNumber = questsManagerScript.GetTotalNumberOfQuests();

        // Event Manager
        eventsManagerScript = eventsManager.GetComponent<EventsManager>();
        eventsTotalNumber = eventsManagerScript.GetTotalNumberOfEvents();

        // System Center Search
        systemCenter = GameObject.FindGameObjectWithTag("SystemsCenter");

        if (usingInspector)
        {
            usingSeed = false;
        }
        else if (usingSeed)
        {
            usingInspector = false;

            randomSun = false;
            red = false;
            blue = false;
            yellow = false;

            randomSunCount = false;
            one = false;
            two = false;
            three = false;

            addMoltenPlanets = 0;
            addEarthLikePlanets = 0;
            addTombPlanets = 0;
            addGasGiantPlanets = 0;
            addFrozenPlanets = 0;

            randomFields = false;
            internalField = false;
            middleField = false;
            externalField = false;

            addHelium = false;
            addGold = false;
            addIron = false;
            addSilicium = false;
            addUranium = false;
            addWater = false;
            addNuclearuel = false;

            minNumberOfPlanets = 0;
            maxNumberOfPlanets = 0;

            minNumberOfPOI = 0;
            maxNumberOfPOI = 0;

            minNumberOfEnemySpawners = 0;
            maxNumberOfEnemySpawners = 0;

            ReadSeed();
        }

        // Suns create
        if (red) { }
        else if (blue) { }
        else if (yellow) { }
        else if (randomSun)
        {
            int r = Random.Range(1, 4);

            switch (r)
            {
                case 1:
                    red = true;
                    break;
                case 2:
                    blue = true;
                    break;
                case 3:
                    yellow = true;
                    break;
            }

            randomSun = false;
        }

        if (one)
        {
            requestedSuns = 1;
            two = false;
            three = false;
            randomSunCount = false;
        }
        else if (two)
        {
            requestedSuns = 2;
            one = false;
            three = false;
            randomSunCount = false;
        }
        else if (three)
        {
            requestedSuns = 3;
            one = false;
            two = false;
            randomSunCount = false;
        }
        else if (randomSunCount)
        {
            requestedSuns = Random.Range(1, 4);
            one = false;
            two = false;
            three = false;
        }

        CreateSun();

        // POI Create
        if (requestedPOI > minNumberOfPOI)
        {
            randomNumberOfPOI = Random.Range(requestedPOI, maxNumberOfPOI);
        }
        else
        {
            randomNumberOfPOI = Random.Range(minNumberOfPOI, maxNumberOfPOI);
        }

        numberOfPOI = randomNumberOfPOI;

        for (int i = 0; i < randomNumberOfPOI; i++)
        {
            POI newPOI = new POI
            {
                id = pOIIdCounter
            };

            pOIIdCounter++;

            CreatePOI(newPOI);
        }

        // Enemy spawners create
        if (requestedEnemySpawners > minNumberOfEnemySpawners)
        {
            randomNumberOfEnemySpawners = Random.Range(requestedEnemySpawners, maxNumberOfEnemySpawners);
        }
        else
        {
            randomNumberOfEnemySpawners = Random.Range(minNumberOfEnemySpawners, maxNumberOfEnemySpawners);
        }

        numberOfEnemySpawners = randomNumberOfEnemySpawners;

        for (int i = 0; i < randomNumberOfEnemySpawners; i++)
        {
            EnemySpawner newEnemySpawner = new EnemySpawner
            {
                id = i
            };

            CreateEnemySpawner(newEnemySpawner);
        }

        // We sum up and remember the number of requested planets.
        if (addEarthLikePlanets > 0)
        {
            requestedPlanets += addEarthLikePlanets;
        }
        if (addTombPlanets > 0)
        {
            requestedPlanets += addTombPlanets;
        }
        if (addMoltenPlanets > 0)
        {
            requestedPlanets += addMoltenPlanets;
        }
        if (addGasGiantPlanets > 0)
        {
            requestedPlanets += addGasGiantPlanets;
        }
        if (addFrozenPlanets > 0)
        {
            requestedPlanets += addFrozenPlanets;
        }

        // We determine the number of planets that we will generate. This is a random variable between the minimum and maximum values. 
        // At the same time, we can not generate fewer planets than was requested.
        if (requestedPlanets > minNumberOfPlanets)
        {
            randomNumberOfPlanets = Random.Range(requestedPlanets, maxNumberOfPlanets);
        }
        else
        {
            randomNumberOfPlanets = Random.Range(minNumberOfPlanets, maxNumberOfPlanets);
        }

        numberOfPlanets = randomNumberOfPlanets;

        // Create a certain number of planets.
        for (int i = 0; i < randomNumberOfPlanets; i++)
        {
            Planet newPlanet = new Planet(); // Create an instance of the class "Planet" as a template.
            CreatePlanet(newPlanet);         // We apply to it a method that determines the final appearance and location of the planet.
        }

        // Asteroids Fields create
        if (internalField || middleField || externalField)
        {
            CreateAsteroidsFields();
        }
        else if (randomFields)
        {
            int r = Random.Range(0, 4);

            AsteroidFieldsRandomization(r);

            randomFields = false;
            CreateAsteroidsFields();
        }

        systemName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        systemID++;
        systemIsCreated = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!systemIsCreated)
        {
            Debug.Log("Update");
            StartCoroutine(Wait());
            systemIsCreated = true;
        }
    }

    // A method that establishes a resource with a specified ID as a frequently encountered resource for the transmitted planet pattern.
    public void SetFrequentResourceType(int resourceTypeIndex, Planet targetPlanet)
    {
        switch (resourceTypeIndex)
        {
            case 1:
                targetPlanet.frequentResource = Planet.resourceTypes.Helium;
                break;
            case 2:
                targetPlanet.frequentResource = Planet.resourceTypes.Iron;
                break;
            case 3:
                targetPlanet.frequentResource = Planet.resourceTypes.Silicium;
                break;
            case 4:
                targetPlanet.frequentResource = Planet.resourceTypes.Gold;
                break;
            case 5:
                targetPlanet.frequentResource = Planet.resourceTypes.Uranium;
                break;
            case 6:
                targetPlanet.frequentResource = Planet.resourceTypes.Nuclear_Fuel;
                break;
            case 7:
                targetPlanet.frequentResource = Planet.resourceTypes.Water;
                break;
        }
    }

    // A method that establishes a resource with a specified ID as a normally encountered resource for the transmitted planet pattern.
    public void SetNormalResourceType(int resourceTypeIndex, Planet targetPlanet)
    {
        switch (resourceTypeIndex)
        {
            case 1:
                targetPlanet.normalResource = Planet.resourceTypes.Helium;
                break;
            case 2:
                targetPlanet.normalResource = Planet.resourceTypes.Iron;
                break;
            case 3:
                targetPlanet.normalResource = Planet.resourceTypes.Silicium;
                break;
            case 4:
                targetPlanet.normalResource = Planet.resourceTypes.Gold;
                break;
            case 5:
                targetPlanet.normalResource = Planet.resourceTypes.Uranium;
                break;
            case 6:
                targetPlanet.normalResource = Planet.resourceTypes.Nuclear_Fuel;
                break;
            case 7:
                targetPlanet.normalResource = Planet.resourceTypes.Water;
                break;
        }
    }

    // A method that establishes a resource with a specified ID as a rarely encountered resource for the transmitted planet pattern.
    public void SetRareResourceType(int resourceTypeIndex, Planet targetPlanet)
    {
        switch (resourceTypeIndex)
        {
            case 1:
                targetPlanet.rareResource = Planet.resourceTypes.Helium;
                break;
            case 2:
                targetPlanet.rareResource = Planet.resourceTypes.Iron;
                break;
            case 3:
                targetPlanet.rareResource = Planet.resourceTypes.Silicium;
                break;
            case 4:
                targetPlanet.rareResource = Planet.resourceTypes.Gold;
                break;
            case 5:
                targetPlanet.rareResource = Planet.resourceTypes.Uranium;
                break;
            case 6:
                targetPlanet.rareResource = Planet.resourceTypes.Nuclear_Fuel;
                break;
            case 7:
                targetPlanet.rareResource = Planet.resourceTypes.Water;
                break;
        }
    }

    // The method that determines the type of planet, based on its distance from the star.
    public void SetPlanetType(float rangeFromSun, Planet targetPlanet)
    {
        // We control the distance to the star, on the basis of this we determine the type of planet. 
        // If there are requirements for planets of different types in one zone, we satisfy them in turn. 
        // When assigning a planet type, we always reduce the number of requested planets of this type.
        if (rangeFromSun < 300)
        {
            targetPlanet.type = Planet.planetTypes.Molten;
            addMoltenPlanets--;
        }
        else if (rangeFromSun >= 300 && rangeFromSun < 500)
        {
            if (addTombPlanets > 0)
            {
                targetPlanet.type = Planet.planetTypes.Tomb;
                addTombPlanets--;
            }
            else
            {
                targetPlanet.type = Planet.planetTypes.Twin_Earth;
                addEarthLikePlanets--;
            }
        }
        else if (rangeFromSun >= 500 && rangeFromSun < 700)
        {
            targetPlanet.type = Planet.planetTypes.Gas_Giant;
            addGasGiantPlanets--;
        }
        else if (rangeFromSun >= 700)
        {
            targetPlanet.type = Planet.planetTypes.Frozen;
            addFrozenPlanets--;
        }
    }

    // A method that randomly assigns resources to a selected planet based on its type.
    public void SetResourcesOfPlanet(Planet targetPlanet, Planet.planetTypes planetType)
    {
        // Each resource has its own ID, on each planet there are three cells for resources.
        int randomFrequentResourceIndex;
        int randomNormalResourceIndex;
        int randomRareResourceIndex;

        // Based on the type of planet...
        switch (planetType)
        {
            // For each cell, we define its resource, randomly generating a digit representing the ID of the resource type, 
            //    using as constraints the limits imposed by the planet type. Then we use a special method to assign it to the pattern of the planet.
            case Planet.planetTypes.Molten:

                if (addIron)
                {
                    randomFrequentResourceIndex = 2;
                    SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);
                    addIron = false;
                }
                else
                {
                    randomFrequentResourceIndex = Random.Range(2, 4);
                    SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);
                }

                randomNormalResourceIndex = Random.Range(2, 5);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(4, 6);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Twin_Earth:

                randomFrequentResourceIndex = Random.Range(7, 8);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(2, 6);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(4, 6);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Gas_Giant:

                randomFrequentResourceIndex = Random.Range(1, 2);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 2);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(1, 2);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Frozen:

                randomFrequentResourceIndex = Random.Range(7, 8);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 3);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(3, 6);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Tomb:

                randomFrequentResourceIndex = Random.Range(5, 6);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 7);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(6, 7);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            default:

                randomFrequentResourceIndex = Random.Range(1, 7);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 7);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(1, 7);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;
        }
    }

    public void PlaceSun(Vector3 sunStartPosition, float scale)
    {
        normalSun.transform.localScale = new Vector3(scale,scale,scale);
        Instantiate(normalSun, sunStartPosition, Quaternion.identity);
    }

    // The method of assigning resources to the planet and placing it in space.
    public void PlacePlanet(Planet planetPrefab, Vector3 randomStartPosition)
    {
        // When choosing an action, we are based on the transmitted type of planet.
        switch (planetPrefab.type)
        {
            // Then we set the type of resources for a given planet, using not a general template, but a template of a specific type of planet.
            // Just in case, we additionally inform her of her type.
            // Directly place an instance of the planet pattern of the required type, in the place that was previously defined.
            case Planet.planetTypes.Molten:
                SetResourcesOfPlanet(moltenPlanet, Planet.planetTypes.Molten);
                moltenPlanet.type = Planet.planetTypes.Molten;
                float moltenPlanetSpeed = Random.Range(0.8f, 1.5f);
                float moltenPlanetSize = Random.Range(5.0f, 8.0f);
                SetPlanetSpeed(moltenPlanet, moltenPlanetSpeed);
                SetPlanetSize(moltenPlanet, moltenPlanetSize);
                moltenPlanet.questIsHere = false;
                moltenPlanet.questID = -1;
                moltenPlanet.eventIsHere = false;
                moltenPlanet.eventID = -1;
                QuestAssigments(moltenPlanet);
                EventAssigments(moltenPlanet);
                moltenPlanet.startPosition = randomStartPosition;
                moltenPlanet.transform.position = randomStartPosition;
                Planet parent = Instantiate(moltenPlanet);
                PlacePOINearPlanet(parent);
                break;
            case Planet.planetTypes.Twin_Earth:
                SetResourcesOfPlanet(twinEarthPlanet, Planet.planetTypes.Twin_Earth);
                twinEarthPlanet.type = Planet.planetTypes.Twin_Earth;
                float twinEarthPlanetSpeed = Random.Range(0.2f, 0.6f);
                float twinEarthPlanetSize = Random.Range(8.0f, 11.0f);
                SetPlanetSpeed(twinEarthPlanet, twinEarthPlanetSpeed);
                SetPlanetSize(twinEarthPlanet, twinEarthPlanetSize);
                twinEarthPlanet.questIsHere = false;
                twinEarthPlanet.questID = -1;
                twinEarthPlanet.eventIsHere = false;
                twinEarthPlanet.eventID = -1;
                QuestAssigments(twinEarthPlanet);
                EventAssigments(twinEarthPlanet);
                twinEarthPlanet.startPosition = randomStartPosition;
                twinEarthPlanet.transform.position = randomStartPosition;
                Planet parent2 = Instantiate(twinEarthPlanet);
                PlacePOINearPlanet(parent2);
                break;
            case Planet.planetTypes.Gas_Giant:
                SetResourcesOfPlanet(gasGiantPlanet, Planet.planetTypes.Gas_Giant);
                gasGiantPlanet.type = Planet.planetTypes.Gas_Giant;
                float gasGiantPlanetSpeed = Random.Range(0.1f, 0.25f);
                float gasGiantSize = Random.Range(16.0f, 51.0f);
                SetPlanetSpeed(gasGiantPlanet, gasGiantPlanetSpeed);
                SetPlanetSize(gasGiantPlanet, gasGiantSize);
                gasGiantPlanet.questIsHere = false;
                gasGiantPlanet.questID = -1;
                gasGiantPlanet.eventIsHere = false;
                gasGiantPlanet.eventID = -1;
                QuestAssigments(gasGiantPlanet);
                EventAssigments(gasGiantPlanet);
                gasGiantPlanet.startPosition = randomStartPosition;
                gasGiantPlanet.transform.position = randomStartPosition;
                Planet parent3 = Instantiate(gasGiantPlanet);
                PlacePOINearPlanet(parent3);
                break;
            case Planet.planetTypes.Frozen:
                SetResourcesOfPlanet(frozenPlanet, Planet.planetTypes.Frozen);
                frozenPlanet.type = Planet.planetTypes.Frozen;
                float frozenPlanetSpeed = Random.Range(0.04f, 0.1f);
                float frozenPlanetSize = Random.Range(4.0f, 11.0f);
                SetPlanetSpeed(frozenPlanet, frozenPlanetSpeed);
                SetPlanetSize(frozenPlanet, frozenPlanetSize);
                frozenPlanet.questIsHere = false;
                frozenPlanet.questID = -1;
                frozenPlanet.eventIsHere = false;
                frozenPlanet.eventID = -1;
                QuestAssigments(frozenPlanet);
                EventAssigments(frozenPlanet);
                frozenPlanet.startPosition = randomStartPosition;
                frozenPlanet.transform.position = randomStartPosition;
                Planet parent4 = Instantiate(frozenPlanet);
                PlacePOINearPlanet(parent4);
                break;
            case Planet.planetTypes.Tomb:
                SetResourcesOfPlanet(tombPlanet, Planet.planetTypes.Tomb);
                tombPlanet.type = Planet.planetTypes.Tomb;
                float tombPlanetSpeed = Random.Range(0.2f, 0.4f);
                float tombPlanetSize = Random.Range(7.0f, 11.0f);
                SetPlanetSpeed(tombPlanet, tombPlanetSpeed);
                SetPlanetSize(tombPlanet, tombPlanetSize);
                tombPlanet.questIsHere = false;
                tombPlanet.questID = -1;
                tombPlanet.eventIsHere = false;
                tombPlanet.eventID = -1;
                QuestAssigments(tombPlanet);
                EventAssigments(tombPlanet);
                tombPlanet.startPosition = randomStartPosition;
                tombPlanet.transform.position = randomStartPosition;
                Planet parent5 = Instantiate(tombPlanet);
                PlacePOINearPlanet(parent5);
                break;
        }
    }

    public void PlaceAsteroid(Vector3 coordinates)
    {
        float asteroidSize = Random.Range(0.5f, 2.0f);
        SetAsteroidSize(asteroid, asteroidSize);
        Instantiate(asteroid, coordinates, Quaternion.identity);
    }

    public void SetPlanetSpeed(Planet targetPlanet, float newSpeed)
    {
        targetPlanet.speed = newSpeed;
    }

    public void SetPlanetSize(Planet targetPlanet, float newSize)
    {
        targetPlanet.size = newSize;
    }

    public void SetAsteroidSize (GameObject asteroid, float newSize)
    {
        asteroid.transform.localScale = new Vector3(newSize, newSize, newSize);
    }

    public void QuestAssigments(Planet targetPlanet)
    {
        for (int i = 0; i < questsTotalNumber; i++)
        {
            bool replasementCheck = false;
            Quest checkingReplasementCondition = null;

            checkingReplasementCondition = questsManagerScript.GetQuest(i);
            checkingReplasementCondition.planet = targetPlanet;
            replasementCheck = checkingReplasementCondition.CheckingPlacementCondition();

            if (replasementCheck)
            {
                Debug.Log("NEW QUEST: " + "ID: " + i + " " + "Time: " + System.DateTime.Now + " Planet: " + targetPlanet);
                targetPlanet.questIsHere = true;
                targetPlanet.questID = i;
                questsManagerScript.QuestPlacement(i);
                numberOfQuests++;
            }
        }
    }

    public void EventAssigments(Planet targetPlanet)
    {
        bool replasementCheck = false;
        int randomEventID = Random.Range(0, eventsTotalNumber);
        LiS_Event checkingReplasementCondition = null;

        checkingReplasementCondition = eventsManagerScript.GetEvent(randomEventID);
        checkingReplasementCondition.planet = targetPlanet;
        replasementCheck = checkingReplasementCondition.CheckingPlacementCondition("Planet");

        if (replasementCheck)
        {
            Debug.Log("NEW EVENT: " + "ID: " + randomEventID + " " + "Time: " + System.DateTime.Now + " Planet: " + targetPlanet);
            targetPlanet.eventIsHere = true;
            targetPlanet.eventID = randomEventID;
            eventsManagerScript.EventPlacement(randomEventID);
            numberOfEvents++;
        }
    }

    public void CreateSun()
    {
        if (requestedSuns == 1)
        {
            one = true;
            numberOfSuns = 1;

            Vector3 sunStartPosition = new Vector3(0, 0, 0);
            float scale = 1.5f;
            PlaceSun(sunStartPosition,scale);
        }
        else if (requestedSuns == 2)
        {
            two = true;
            numberOfSuns = 2;

            Vector3 sunStartPosition1 = new Vector3(-30, -30, 0);
            float scale1 = 0.6f;
            PlaceSun(sunStartPosition1, scale1);

            Vector3 sunStartPosition2 = new Vector3(30, 30, 0);
            float scale2 = 0.3f;
            PlaceSun(sunStartPosition2, scale2);
        }
        else if (requestedSuns == 3)
        {
            three = true;
            numberOfSuns = 3;

            Vector3 sunStartPosition1 = new Vector3(-30, -30, 0);
            float scale1 = 0.4f;
            PlaceSun(sunStartPosition1, scale1);

            Vector3 sunStartPosition2 = new Vector3(30, -10, 0);
            float scale2 = 0.3f;
            PlaceSun(sunStartPosition2, scale2);

            Vector3 sunStartPosition3 = new Vector3(0, 35, 0);
            float scale3 = 0.2f;
            PlaceSun(sunStartPosition3, scale3);
        }

        randomSunCount = false;
    }

    // Main method for creating a planet. Randomly generates a starting position for it and calculates the distance to the sun. 
    // Then it calls methods that establish the type of planet and place it in space.
    public void CreatePlanet(Planet planet)
    {
        Vector3 randomStartPosition = new Vector3();

        // If there are requirements to create a planet of a certain type, 
        //    then as coordinates we use the coordinates of the zone in which it may appear. Otherwise, we determine the coordinates randomly.
        if (addEarthLikePlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(300.0f, 350.0f), Random.Range(300.0f, 350.0f), 0);
        }
        else if (addMoltenPlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(80.0f, 85.0f), Random.Range(80.0f, 85.0f), 0);
        }
        else if (addTombPlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(300.0f, 350.0f), Random.Range(300.0f, 350.0f), 0);
        }
        else if (addGasGiantPlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(450.0f, 520.0f), Random.Range(450.0f, 520.0f), 0);
        }
        else if (addFrozenPlanets > 0)
        {
            randomStartPosition = new Vector3((Random.Range(900.0f, 1000.0f)), (Random.Range(900.0f, 1000.0f)), 0);
        }
        else
        {
            randomStartPosition = new Vector3(Random.Range(-1000.0f, 1000.0f), Random.Range(-1000.0f, 1000.0f), 0);
        }

        // Calculate the distance from the planet to the sun.
        float rangeFromSun = Vector2.Distance(new Vector2(randomStartPosition.x, randomStartPosition.y), new Vector2(0, 0));

        // Call the method that determines the type of planet.
        SetPlanetType(rangeFromSun, planet);

        // Call the method that places the planet in space.
        PlacePlanet(planet, randomStartPosition);
    }

    public void CreateAsteroidsFields()
    {
        if (internalField)
        {
            for (int i = 0; i <= 50; i++)
            {
                Vector3 randomAsteroidStartPosition = new Vector3(Random.Range(100.0f, 120.0f), Random.Range(100.0f, 120.0f), 0);
                PlaceAsteroid(randomAsteroidStartPosition);
            }
        }
        if (middleField)
        {
            for (int i = 0; i <= 150; i++)
            {
                Vector3 randomAsteroidStartPosition = new Vector3(Random.Range(200.0f, 220.0f), Random.Range(200.0f, 220.0f), 0);
                PlaceAsteroid(randomAsteroidStartPosition);
            }
        }
        if (externalField)
        {
            for (int i = 0; i <= 200; i++)
            {
                Vector3 randomAsteroidStartPosition = new Vector3(Random.Range(350.0f, 400.0f), Random.Range(350.0f, 400.0f), 0);
                PlaceAsteroid(randomAsteroidStartPosition);
            }
        }
    }

    public void CreatePOI(POI pOI)
    {
        POITypeDefinition(pOI);

        for (int i = 0; i < pOIInSpace; i++)
        {
            PlacePOIInSpace(pOI);
        }

        pOIInSpace = 0;
    }

    public void POITypeDefinition(POI pOI)
    {
        int random = Random.Range(1, 11);

        if (random <= 5)
        {
            pOI.type = "NearPlanet";
            pOINearPlanet++;
            pOIIdCounter--;
        }
        else if (random > 5)
        {
            pOI.type = "Space";
            pOIInSpace++;
        }
    }

    public void PlacePOINearPlanet(Planet parent)
    {
        if (pOINearPlanet > 0)
        {
            POI newPOI = new POI
            {
                type = "NearPlanet",
                id = pOIIdCounter
            };

            pOIIdCounter++;

            pOIPrefab.id = newPOI.id;
            pOIPrefab.type = newPOI.type;
            pOIPrefab.placed = true;
            pOIPrefab.parentPlanet = parent;

            Instantiate(pOIPrefab);

            pOINearPlanet--;
        }
    }

    public void PlacePOIInSpace(POI pOI)
    {
        Vector3 pOICoordinate = new Vector3();

        int x = Random.Range(-500, 500);
        int y = Random.Range(-500, 500);
        int z = 0;

        pOICoordinate.x = x;
        pOICoordinate.y = y;
        pOICoordinate.z = z;

        pOIPrefab.id = pOI.id;
        pOIPrefab.type = pOI.type;
        pOIPrefab.transform.position = pOICoordinate;
        pOIPrefab.placed = true;

        Instantiate(pOIPrefab);
    }

    public void CreateEnemySpawner(EnemySpawner enemySpawner)
    {
        Vector3 enemySpawnerCoordinate = new Vector3();

        int x = Random.Range(-500, 500);
        int y = Random.Range(-500, 500);
        int z = 0;

        enemySpawnerCoordinate.x = x;
        enemySpawnerCoordinate.y = y;
        enemySpawnerCoordinate.z = z;

        enemySpawnerPrefab.id = enemySpawner.id;
        enemySpawnerPrefab.transform.position = enemySpawnerCoordinate;
        enemySpawnerPrefab.placed = true;

        Instantiate(enemySpawnerPrefab);
    }

    public void ReadSeed()
    {
        seed = seedGeneratorScript.GetSeed();

        int stage = 0;
        int asteroidFieldsCounter = 0;

        string sSunSettings = null;
        string sSunsCount = null;
        string sAsteroidsCount = null;
        string sMinNumberOfPlanets = null;
        string sMaxNumberOfPlanets = null;
        string sMinNumberOfPOI = null;
        string sMaxNumberOfPOI = null;
        string sMinNumberOfEnemySpawners = null;
        string sMaxNumberOfEnemySpawners = null;

        for (int i = 0; i < seed.Length; i++)
        {
            if (seed[i] == '#')
            {
                break;
            }

            if (seed[i] == '/')
            {
                stage++;
            }

            switch (stage)
            {
                case 0:
                    sSunSettings += seed[i];
                    break;
                case 1:
                    if (seed[i] != '/')
                    {
                        sSunsCount += seed[i];
                    }
                    break;
                case 2:
                    if (seed[i] != '/')
                    {
                        sMinNumberOfPlanets += seed[i];
                    }
                    break;
                case 3:
                    if (seed[i] != '/')
                    {
                        sMaxNumberOfPlanets += seed[i];
                    }
                    break;
                case 4:
                    if (seed[i] != '/')
                    {
                        sAsteroidsCount += seed[i];
                    }
                    break;
                case 5:
                    if (seed[i] != '/')
                    {
                        sMinNumberOfPOI += seed[i];
                    }
                    break;
                case 6:
                    if (seed[i] != '/')
                    {
                        sMaxNumberOfPOI += seed[i];
                    }
                    break;
                case 7:
                    if (seed[i] != '/')
                    {
                        sMinNumberOfEnemySpawners += seed[i];
                    }
                    break;
                case 8:
                    if (seed[i] != '/')
                    {
                        sMaxNumberOfEnemySpawners += seed[i];
                    }
                    break;
            }
        }

        minNumberOfPlanets = int.Parse(sMinNumberOfPlanets);
        maxNumberOfPlanets = int.Parse(sMaxNumberOfPlanets);

        minNumberOfPOI = int.Parse(sMinNumberOfPOI);
        maxNumberOfPOI = int.Parse(sMaxNumberOfPOI);

        minNumberOfEnemySpawners = int.Parse(sMinNumberOfEnemySpawners);
        maxNumberOfEnemySpawners = int.Parse(sMaxNumberOfEnemySpawners);

        switch (sSunSettings)
        {
            case "red":
                red = true;
                break;
            case "blue":
                blue = true;
                break;
            case "yellow":
                yellow = true;
                break;
        }

        switch (sSunsCount)
        {
            case "1":
                one = true;
                break;
            case "2":
                two = true;
                break;
            case "3":
                three = true;
                break;
        }

        asteroidFieldsCounter = int.Parse(sAsteroidsCount);
        AsteroidFieldsRandomization(asteroidFieldsCounter);
    }

    public void AsteroidFieldsRandomization(int random)
    {
        int asteroidRandom1 = Random.Range(1, 4);
        int asteroidRandom2 = Random.Range(1, 4);
        int asteroidRandom3 = Random.Range(1, 4);

        switch (random)
        {
            case 0:
                break;
            case 1:
                switch (asteroidRandom1)
                {
                    case 1:
                        internalField = true;
                        break;
                    case 2:
                        middleField = true;
                        break;
                    case 3:
                        externalField = true;
                        break;
                }
                break;
            case 2:
                switch (asteroidRandom1)
                {
                    case 1:
                        internalField = true;
                        break;
                    case 2:
                        middleField = true;
                        break;
                    case 3:
                        externalField = true;
                        break;
                }
                switch (asteroidRandom2)
                {
                    case 1:
                        if (internalField)
                        {
                            middleField = true;
                        }
                        else
                        {
                            internalField = true;
                        }
                        break;
                    case 2:
                        if (middleField)
                        {
                            internalField = true;
                        }
                        else
                        {
                            middleField = true;
                        }
                        break;
                    case 3:
                        if (externalField)
                        {
                            middleField = true;
                        }
                        else
                        {
                            externalField = true;
                        }
                        break;
                }
                break;
            case 3:
                internalField = true;
                middleField = true;
                externalField = true;
                break;
        }
    }

    public IEnumerator Wait()
    {
        Debug.Log("Wait");
        yield return new WaitForSeconds(5);
        Start();
    }
}
