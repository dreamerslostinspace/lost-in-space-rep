﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POI : MonoBehaviour
{
    public int id;
    public string type;
    public bool placed;
    public LiS_Event liS_Event;
    public Planet parentPlanet;
    public int deviation;

    // Start is called before the first frame update
    void Start()
    {
        deviation = Random.Range(5, 10);

        if (parentPlanet != null)
        {
            gameObject.transform.SetParent(parentPlanet.transform);
        }

        gameObject.transform.position = new Vector3 
            (parentPlanet.transform.position.x + deviation, parentPlanet.transform.position.y + deviation, parentPlanet.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
