﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// assign this script to every button in the scene
// just manages sound and visual behaviour
public class MainMenuButtonBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
{
    public AudioSource audioSource;
    public AudioClip buttonHoverAudio;
    public AudioClip buttonClickAudio;

    public TextMeshProUGUI buttonText;

    private void SetTextColor()
    {
        if (buttonText.color == Color.black)
        {
            buttonText.color = Color.white;
        }
        else if (buttonText.color == Color.white)
        {
            buttonText.color = Color.black;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        SetTextColor();

        audioSource.clip = buttonClickAudio;
        audioSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SetTextColor();

        audioSource.clip = buttonHoverAudio;
        audioSource.Play();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        buttonText.color = Color.black;
    }
}
