﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// assign this script to every button in the scene
// just manages sound and visual behaviour
public class PauseMenuUIButtonBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
{
    public AudioSource audioSource;
    public AudioClip buttonHoverAudio;
    public AudioClip buttonClickAudio;

    public TextMeshProUGUI buttonText;

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonText.color = Color.white;

        audioSource.clip = buttonClickAudio;
        audioSource.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        buttonText.color = Color.black;

        audioSource.clip = buttonHoverAudio;
        audioSource.Play();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        buttonText.color = Color.white;
    }
}
