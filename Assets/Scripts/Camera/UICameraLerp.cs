﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraLerp : MonoBehaviour
{
    
    private Camera menuCamera;
    private Transform cameraTransform;
    public Transform[] views;    

    private float lerpTime = 1f;
    public float currentLerpTime;
    
    // camera position
    public Transform cameraTargetTransform;
    
    // Start is called before the first frame update
    void Start()
    {
        menuCamera = GameObject.Find("Menu Camera").GetComponent<Camera>();
        cameraTransform = menuCamera.transform;
        cameraTargetTransform = menuCamera.transform;
    }

    // Update is called once per frame
    void Update()
    {
        //increment
        currentLerpTime += Time.deltaTime / 2;
        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = lerpTime;
        }
        
        // lerp
        float percentage = currentLerpTime / lerpTime;
        cameraTransform.position = Vector3.Lerp(cameraTransform.position, cameraTargetTransform.position, percentage);
    }
}
