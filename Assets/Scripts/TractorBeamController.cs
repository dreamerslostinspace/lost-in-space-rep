﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TractorBeamController : MonoBehaviour  
{
    public LineRenderer beam;
    public float maxRange;
    public float minRange;
    public float strenght;
    private GameObject lastPulled;


    public InventoryDragNDropHandler dragHandler;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (dragHandler.isDragging())
        {
            beam.positionCount = 1;
            lastPulled = null;
            return;
        }


        if (Input.GetMouseButton(0))
        {
            beam.SetPosition(0, transform.position);

            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos = new Vector3(mousePos.x, mousePos.y);

            Vector2 target;
            float range;

            if (lastPulled == null)
            {
                target = Random.insideUnitCircle * 0.5f;
                target = new Vector2(target.x + 1, target.y + 1);
                target *= (mousePos - transform.position).normalized;

                range = Random.Range(minRange, maxRange);
            }
            else
            {
                target = (lastPulled.transform.position - transform.position).normalized;
                range = maxRange;
            }

            beam.positionCount = 2;

            RaycastHit hitInfo;
            bool hit = Physics.Raycast(transform.position, target, out hitInfo, range, LayerMask.GetMask("Item"));
            if (hit)
            {
                lastPulled = hitInfo.collider.gameObject;
                hitInfo.collider.attachedRigidbody.AddForce((transform.position - hitInfo.collider.transform.position).normalized * strenght);
                beam.SetPosition(1, hitInfo.point);
            }
            else
            {
                beam.SetPosition(1, target * range);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (lastPulled != null)
            {
                lastPulled.GetComponent<Rigidbody>().velocity = Vector3.zero;
                lastPulled.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                lastPulled = null;
            }
            beam.positionCount = 1;
        }
    }
}
