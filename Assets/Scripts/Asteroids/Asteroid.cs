﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : SpaceObject
{
    [Header("Asteroid")]
    public double distanceForMining;

    public float dangerbyHit;

    public float timeLeft;
    public float timeToDisapaer;

    public bool flowEnd;
    public bool mined = false;
    public bool isKilling;
    public bool isFall;

    void Start()
    {
        material = GenerateMaterial();
        timeToDisapaer = 30;
        timeLeft = timeToDisapaer;

        speed = Random.Range(0.4f, 1.50f);
        distanceForMining = 1.5;
        Move();
    }

    void Update()
    {
        if (!Environment.OnScreen(transform.position))
            timeLeft -= Time.deltaTime;
        else
            timeLeft = timeToDisapaer;

        if (timeLeft <= 0)
            Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            print("hit");
        }
    }

    /* public void HitTheShip()
     {
         if (isKilling)  //for killing Asteroids
         {
             if (Environment.GetInteraction(player, gameObject, distance, gameObject.transform.localScale.x) && !flowEnd)
             {
                 float shipSpeed = player.GetComponent<Ship>().movementDirection;
                 int rotation = GetRotationToShip(player, gameObject);

                 player.GetComponent<GameController>().HitPlayer(ComputeDamage((float)rotation, ((float)speed / 100), (shipSpeed / 2.5f)));
                 flowEnd = true;

             }
         }
         else   //for asteroids to mining
         {
             if (Environment.GetInteraction(player, gameObject, distance, gameObject.transform.localScale.x) && !flowEnd) //stop the asteroid
             {
                 flowEnd = true;
             }
             if (Input.GetKeyUp("f"))
             {
                 if (material != null)
                 {
                     if (Environment.GetInteraction(player, gameObject, distanceForMining, gameObject.transform.localScale.x))
                     {
                         if(GameObject.Find("Inventory").GetComponent<Inventory>().AddItem(material.Name, material.Amount))
                         {
                             AudioClip pickupSound = Resources.Load<AudioClip>("Sounds/comet pickup");
                             player.GetComponent<AudioSource>().PlayOneShot(pickupSound);
                             flowEnd = true;
                             mined = true;
                         }
                     }
                 }
             }
         }
     }*/

    //public int ComputeDamage(float rotation, float normalizedAsteroidSpeed /*0.8-1.6*/, float normalizedShipSpeed /*-0.8-2*/)
    /*{
        float damage; int damageShipFactor;
        
        if (rotation < 90)
        {
            damage = normalizedAsteroidSpeed + (normalizedShipSpeed * ((90 - rotation) / 90));
        }
        else
        {
            if (rotation < 180)
            {
                rotation -= 90;
                damage = normalizedAsteroidSpeed - (normalizedShipSpeed * (rotation / 90));
            }
            else
            {
                if (rotation < 270)
                {
                    rotation -= 180;
                    damage = normalizedAsteroidSpeed - (normalizedShipSpeed * ((90 - rotation) / 90));
                }
                else
                {
                    rotation -= 270;
                    damage = normalizedAsteroidSpeed + (normalizedShipSpeed * (rotation / 90));
                }
            }
        }
    
        damage *= dangerbyHit;

        return (int)damage;
    }*/

    private Item GenerateMaterial(int indexer = 0) //Generating material for mining asteroids.
    {
        if (indexer == 0)
        {
            int rnd = Random.Range(0, 100);
            if (rnd < 37) //Basic2 materials
            {
                indexer = Random.Range(3, 5);
            }
            else //Basic1 materials
            {
                indexer = Random.Range(1, 3);
            }       
        }

        Item item = new Item(GameObject.Find("Inventory").GetComponent<ItemDatabase>().items[indexer].Name);
        item.SetAmount(1);
        return item;
    }
}

