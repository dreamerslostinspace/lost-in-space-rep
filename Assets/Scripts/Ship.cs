﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [Header("Attributes")]
    public float speed;             // Speed factor
    public float rotationSpeed;     // Rotation speed factor

    [Header("Limits")]
    public float strafeLimiter;

    [Header("ReadOnly")]
    public Vector3 velocity;
    public bool shipNotMoving;

    public float forwardSpeed; // The direction of movement
    public float rotationDirection; // The direction of rotation
    public float strafeMovement;    // How much you can move to left/right side?

    public Transform target;        // Component to get target coordinates


    public bool foodReplenishment;  // The determinant of whether food is replenished near the planet
    public bool speedUpgrade = false;


    private Rigidbody rb; // For physics
    private GameController gC;       // To access health and nutrition indicators


    // Use this for initialization
    void Start ()
    {
        rb = this.GetComponent<Rigidbody>();        // We take a component from our object
        gC = this.GetComponent<GameController>();       // Get GameController

        foodReplenishment = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if game is paused stop everything
        if (gC.paused)
            return;

        velocity = rb.velocity;

        if (velocity.Equals(Vector3.zero))
            shipNotMoving = true;

        /*if (gC.fuelCount <= 0)
        {
            maxForwardSpeed = 1.5f;
            maxBackwardSpeed = -0.5f;
            rotationSpeed = 0.5f;
        }
        else
        {
            if (speedUpgrade)
            {
                maxForwardSpeed = 5f;
                maxBackwardSpeed = -2f;
                rotationSpeed = 2f;
            }
            else
            {
                maxForwardSpeed = 3f;
                maxBackwardSpeed = -1f;
                rotationSpeed = 1f;
            }
        }*/

        Controls();    // Moving.
    }

    private void Controls()
    {
        //moving forward or backard
        float verticalAxis = Input.GetAxis("Vertical"); // Extract vertical axis information

        forwardSpeed = verticalAxis * speed; // Determine the direction of movement, 
                                              //  multiplying the number extracted from the control by the speed factor

        /* We change the speed of the rigidbody, which gives the engine a command to move the object.
            For this purpose, the position of the object along the Y axis is inverted and multiplied by the direction of motion. */
        rb.AddForce(transform.up * forwardSpeed, ForceMode.Force);
        //----------------

        //moving sideways (strafing) 
        if (Input.GetKey("q"))              //right strafing
            strafeMovement += speed / strafeLimiter;
        else if (Input.GetKey("e"))         //left strafing
            strafeMovement -= speed / strafeLimiter;
        else
            strafeMovement = 0f;

        rb.AddForce(-transform.right * strafeMovement, ForceMode.Force);
        //------------

        //rotation
        float horizontalAxis = Input.GetAxis("Horizontal"); // Extract horizontal axis information

        rotationDirection = horizontalAxis * rotationSpeed; // Determine the direction of rotation, 
                                                            //  multiplying the number extracted from the control by the rotation speed factor
        rb.AddTorque(-transform.forward * rotationDirection, ForceMode.Force);
        //transform.Rotate(0, 0, -rotationDirection);      // Rotate the object according to the direction of rotation

        //------------

        //braking 
        if (Input.GetAxis("Jump") > 0) // If pressed the spacebar
        {
            rb.AddForce(-rb.velocity * 2f, ForceMode.Force);
            rb.AddTorque(-rb.angularVelocity, ForceMode.Force);

            if (Mathf.Abs(rb.velocity.x) < 0.1f && Mathf.Abs(rb.velocity.y) < 0.1f && Mathf.Abs(rb.velocity.z) < 0.05f)
                rb.velocity = Vector3.zero;
            if (Mathf.Abs(rb.angularVelocity.x) < 0.05f && Mathf.Abs(rb.angularVelocity.y) < 0.05f && Mathf.Abs(rb.angularVelocity.z) < 0.05f)
                rb.angularVelocity = Vector3.zero;
        }
    }

    public void Stop()
    {
        rb.velocity = new Vector3(0, 0, 0);
        rb.angularVelocity = new Vector3(0, 0, 0);
    }

    void OnCollisionEnter(Collision collision) // It is executed when the object's collider is in contact with another collider
    {
        GameObject.Find("SpaceShip").GetComponent<GameController>().HitPlayer(1); // Reduce HP  

        if (collision.gameObject.tag == "AsteroidBelt")
        {
            if (!gameObject.GetComponent<GameController>().asteroidShieldOpen)
            GameObject.Find("SpaceShip").GetComponent<GameController>().HitPlayer(100); // Reduce HP  
        }
    }

    void OnTriggerEnter(Collider collision) // It is executed when the object's collider is in contact with another collider marked "Trigger"
    {
        if(collision.tag == "Planet" || collision.tag == "Living Planet") // If with the planet (including live)
        {
            target = collision.transform;                                 // The coordinates of what we encountered
            transform.SetParent(target);                                  // Set the object we encountered as the parent
            rb.AddRelativeForce(Vector3.forward * 100);          // Direct the force on the ship to attract it to the parent element
        }

        if(collision.tag == "Living Planet") // If with a living planet
        {
            foodReplenishment = true;        // Then put a flag that you can replenish food stocks
        }
    }

    void OnTriggerExit(Collider collision) // It is executed when the object's collider stops contacting the collider marked as “Trigger”
    {
        if (collision.tag == "Planet")         // If with a planet
        {
            target = null;                     // Clear
            this.transform.parent = null;      // Breaking the link with the parent element
        }

        if (collision.tag == "Living Planet")  // If with a living planet
        {
            foodReplenishment = false;         // Then remove the flag that you can replenish food supplies
            target = null;                     // Clear
            this.transform.parent = null;      // Breaking the link with the parent element
        }

        if (collision.tag == "AsteroidBelt")         
        {
            target = null;                     // Clear
            this.transform.parent = null;      // Breaking the link with the parent element
        }
    }
}
