﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEvent1 : LiS_Event
{
    public override bool CheckingPlacementCondition(string caller)
    {
        if (planet.eventIsHere || fired == true || placed == true || caller != "Planet")
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public override bool CheckingStartCondition()
    {
        if (fired == true)
        {
            return false;
        }

        if (placed)
        {
            // Here is the code that must be executed BEFORE waiting.

            StartCoroutine(Wait());

            // Here is the code that should be executed WHILE waiting.
        }

        return false;
    }

    public override IEnumerator Wait()
    {
        yield return new WaitForSeconds(5);
        Fired();
    }
}
