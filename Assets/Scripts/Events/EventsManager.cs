﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsManager : MonoBehaviour
{
    [SerializeField]
    LiS_Event[] events;

    public bool CheckPlasementCondition(int id, string caller)
    {
        bool check = events[id].CheckingPlacementCondition(caller);

        if (check)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckStartCondition(int id)
    {
        bool check = events[id].CheckingStartCondition();

        if (check)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void EventPlacement(int id)
    {
        events[id].Placement();
    }

    public void EventFired(int id)
    {
        events[id].Fired();
    }

    public void AddNewQuest(int id)
    {
        events[id].AddNewQuest();
    }

    public void AddJournalEntry(int id)
    {
        events[id].AddJournalEntry();
    }

    public void Effects(int id)
    {
        events[id].Effects();
    }

    public int GetTotalNumberOfEvents()
    {
        return events.Length;
    }

    public LiS_Event GetEvent(int id)
    {
        return events[id];
    }
}
