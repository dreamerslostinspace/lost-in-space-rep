﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiS_Event : MonoBehaviour
{
    public int id;
    public string title;
    public bool placed = false;
    public bool fired = false;
    public EventsManager eventsManager;
    public QuestsManager questsManager;
    public GameObject place;
    public GameObject player;
    public Ship ship;

    public Planet planet = null;
    public bool playerContact;
    public bool testFlag = false;

    public virtual void Start()
    {
        player = GameObject.Find("SpaceShip");
        ship = player.GetComponent<Ship>();
    }

    public virtual void Update()
    {
        CheckingStartCondition();
    }

    public virtual bool CheckingPlacementCondition(string caller)
    {
        if (planet.eventIsHere || fired == true ||  placed == true)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public virtual bool CheckingStartCondition()
    {
        if (fired == true)
        {
            return false;
        }

        return false;
    }

    public virtual void Placement()
    {
        this.placed = true;
    }

    public virtual void Fired()
    {
        this.fired = true;
    }

    public virtual void AddNewQuest() { }
    public virtual void AddJournalEntry() { }
    public virtual void Effects() { }

    public virtual IEnumerator Wait()
    {
        yield return new WaitForSeconds(5); // We are waiting for 5 seconds.

        // Here is the code that is executed AFTER waiting.
    }
}