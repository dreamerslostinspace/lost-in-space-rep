﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEvent2 : LiS_Event
{
    public override void Start()
    {
        base.Start();
        place = player;
        placed = true;
    }

    public override bool CheckingPlacementCondition(string caller)
    {
        return false;
    }

    public override bool CheckingStartCondition()
    {
        if (fired == true)
        {
            return false;
        }

        if (ship.forwardSpeed > 0)
        {
            Fired();
            return true;
        }

        return false;
    }
}
