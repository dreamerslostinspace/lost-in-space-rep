﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEvent3 : LiS_Event
{
    public override bool CheckingPlacementCondition(string caller)
    {
        if (planet.eventIsHere || fired == true || placed == true || caller != "POI")
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
