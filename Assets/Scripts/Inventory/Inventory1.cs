﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory1 : MonoBehaviour
{
    public Grid invGrid;

    public int width;
    public int height;

    public List<GameObject> items;

    public GameObject highlightCellPrefab;

    // Start is called before the first frame update
    void Start()
    {
        invGrid = GetComponent<Grid>();
        for(int w = -width/2; w < width/2; w++)
            for(int h = -height/2; h < height/2; h++)
            {
                Vector3 pos = invGrid.GetCellCenterLocal(invGrid.LocalToCell(new Vector3((int)w * invGrid.cellSize.x, (int)h * invGrid.cellSize.y, 0)));
                Instantiate(highlightCellPrefab, pos, Quaternion.identity, transform);
            }
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<BoxCollider>().size = new Vector3(width, height, 0.5f);
    }

    void OnTriggerEnter(Collider collision)
    {
        GameObject go = collision.gameObject;
        if (!items.Contains(go))
            items.Add(go);

        //go.GetComponent<MeshRenderer>().bounds.

    }

    void OnTriggerExit(Collider collision)
    {
        GameObject go = collision.gameObject;
        if (items.Contains(go))
            items.Remove(go);
    }

    public void SnapToGrid(GameObject item)
    {
        Vector3 gridPos = invGrid.WorldToCell(item.transform.position);
        item.transform.position = new Vector3(gridPos.x + 0.5f, gridPos.y + 0.5f);
    }
}
