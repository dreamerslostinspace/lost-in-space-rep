﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class IInventoryObject : MonoBehaviour
{
    public bool inInventory;
    public bool canBePlaced;

    private int ITEMLAYERMASK = 14;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == ITEMLAYERMASK)
        {
            canBePlaced = false;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == ITEMLAYERMASK)
        {
            canBePlaced = true;
        }
    }
}
