﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDragNDropHandler : MonoBehaviour
{
    public Vector3 dragStartPosition;
    public GameObject draggedItem;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        OnBeginDrag(origin);
        OnDrag(origin);
        OnEndDrag(origin);
    }

    
    
    private void OnBeginDrag(Vector3 origin)
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))   //On Begin Drag
        {
            RaycastHit hitInfo;

            bool hit = Physics.Raycast(origin, Vector3.forward, out hitInfo, 100, LayerMask.GetMask("Item"));
            if (hit && !hitInfo.collider.isTrigger)
            {
                draggedItem = hitInfo.collider.gameObject;
                dragStartPosition = draggedItem.transform.position;
            }
        }
    }

    private void OnDrag(Vector3 origin)
    {
        if (Input.GetKey(KeyCode.Mouse0))       //On Drag
        {
            if (isDragging())
            {
                draggedItem.transform.position = new Vector3(origin.x, origin.y, 0f);

                RaycastHit hitInfo;
                bool hit = Physics.Raycast(origin, Vector3.forward, out hitInfo, 100, LayerMask.GetMask("Inventory"));
                if (hit)
                {
                    hitInfo.collider.gameObject.GetComponent<Inventory1>().SnapToGrid(draggedItem);
                }
            }
        }
    }

    private void OnEndDrag(Vector3 origin)
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))     //On End Drag
        {

            if (isDragging())
            {
                RaycastHit hitInfo;
                bool hit = Physics.Raycast(origin, Vector3.forward, out hitInfo, 100, LayerMask.GetMask("Inventory"));
                if (hit && draggedItem.GetComponent<IInventoryObject>().canBePlaced)
                {
                    hitInfo.collider.gameObject.GetComponent<Inventory1>().SnapToGrid(draggedItem);
                    draggedItem.GetComponent<IInventoryObject>().inInventory = true;
                }
                else
                {
                    if (draggedItem.GetComponent<IInventoryObject>().inInventory || !draggedItem.GetComponent<IInventoryObject>().canBePlaced)
                        draggedItem.transform.position = dragStartPosition;
                }
                draggedItem = null;
            }
        }
    }

    public bool isDragging()
    {
        return draggedItem != null;
    }

}
