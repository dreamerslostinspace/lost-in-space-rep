﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryHighlightCell : MonoBehaviour
{
    public int itemCount;
    private bool wasDragging;

    [Header("Highlight")]
    public Color occupiedCell;
    public Color freeCell;
    public Color defaultCell;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (wasDragging && !transform.parent.GetComponent<InventoryDragNDropHandler>().isDragging())
            GetComponent<MeshRenderer>().material.color = defaultCell;  //fade out

        wasDragging = transform.parent.GetComponent<InventoryDragNDropHandler>().isDragging();
    }

    public void OnTriggerEnter(Collider collider)
    {

        itemCount++;
        if (transform.parent.GetComponent<InventoryDragNDropHandler>().isDragging())
        {
            if (itemCount > 1)
            {
                GetComponent<MeshRenderer>().material.color = occupiedCell;  //red
                //collider.GetComponent<IInventoryObject>().canBePlaced = false;
            }
            else
                GetComponent<MeshRenderer>().material.color = freeCell;  //green
        }
    }

    public void OnTriggerExit(Collider collider)
    {
        itemCount--;
        GetComponent<MeshRenderer>().material.color = defaultCell;  //fade out
    }
}
